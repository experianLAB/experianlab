provider "aws" {
  region                  = "us-east-2"
  profile                 = "terraform-user"
}
resource "aws_vpc" "environmentVPC" {
  cidr_block       = "10.17.34.0/24"
  instance_tenancy = "dedicated"

  tags = {
    Name = "environmentVPC"
  }
}
resource "aws_subnet" "appSubnet1" {
  vpc_id     = "${aws_vpc.environmentVPC.id}"
  cidr_block = "10.17.34.0/25"

  tags = {
    Name = "appSubnet1"
  }
}

resource "aws_subnet" "appSubnet2" {
  vpc_id     = "${aws_vpc.environmentVPC.id}"
  cidr_block = "10.17.34.128/25"

  tags = {
    Name = "appSubnet2"
  }
}

resource "aws_instance" "webserver" {
  ami           = "ami-916f59f4"
  instance_type = "t2.micro"

  tags = {
    Name = "wedserver"
  }
}

resource "aws_instance" "appserver" {
  ami           = "ami-916f59f4"
  instance_type = "t2.micro"

  tags = {
    Name = "appserver"
  }
}

resource "aws_db_instance" "appDB" {
  name                = "mydb"
  identifier          = "appDB"
  instance_class      = "db.t2.micro"
  engine              = "postgres"
  username            = "charles"
  password            = "password123"
  allocated_storage   = 20
  skip_final_snapshot = true
}
      
